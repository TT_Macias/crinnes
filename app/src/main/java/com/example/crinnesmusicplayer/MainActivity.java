package com.example.crinnesmusicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView EntrarButton;
    TextView RegistroButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       EntrarButton = (TextView) findViewById(R.id.button);
       EntrarButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent i = new Intent(MainActivity.this, InicioSesion.class);
               startActivity(i);


           }
       });

       RegistroButton =(TextView) findViewById(R.id.button2);
       RegistroButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent j = new Intent(MainActivity.this , Registro.class);
               startActivity(j);
           }
       });


    }
}
